## frontend-test
Frontend test (HTML5, CSS, Javascript)

## Frontend template
  - Frontend template using HTML5, CSS3 (SCSS), Gulp techniques

## Installation
### Install Node.js
  - Download [Node.js](http://nodejs.org)

### Install Gulp
  - Open Command Line and run
    * npm install -g gulp
    * npm install gulp-sass
    * npm install browser-sync

## Development
  - gulp watch